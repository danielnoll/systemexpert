<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Regulations</name>
  <width>1660</width>
  <height>1150</height>
  <background_color>
    <color name="Transparent" red="255" green="255" blue="255" alpha="0">
    </color>
  </background_color>
  <actions>
  </actions>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey03-titlebar_8</name>
    <y>1050</y>
    <width>820</width>
    <height>90</height>
    <line_width>0</line_width>
    <background_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <macros>
      <deriv>$(pv_group)::PID_Exchanger_Td</deriv>
      <integ>$(pv_group)::PID_Exchanger_Ti</integ>
      <measval>$(pv_group):WtrC-TT-010:Temperature</measval>
      <plot1name>PID_Exchanger_TT10-S</plot1name>
      <plot1ypv>$(pv_group)::PID_Exchanger_TT10-S</plot1ypv>
      <plot2name>WtrC-TT-010:Temperature</plot2name>
      <plot2ypv>$(pv_group):WtrC-TT-010:Temperature</plot2ypv>
      <plot3name>WtrC-PCV-010:Opening</plot3name>
      <plot3ypv>$(pv_group):WtrC-PCV-010:Opening</plot3ypv>
      <propor>$(pv_group)::PID_Exchanger_Kp</propor>
      <setpoint>$(pv_group)::PID_Exchanger_TT10-S</setpoint>
      <title>Exchanger regulation loop</title>
      <valve>$(pv_group):WtrC-PCV-010:Opening</valve>
      <yaxis1>°C</yaxis1>
      <yaxis1_max>35</yaxis1_max>
      <yaxis1_min>20</yaxis1_min>
    </macros>
    <file>EPICS-RegField.bob</file>
    <width>820</width>
    <height>350</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_1</name>
    <macros>
      <deriv>$(pv_group)::PID_VaneT_Td</deriv>
      <integ>$(pv_group)::PID_VaneT_Ti</integ>
      <measval>$(pv_group):WtrC-TT-020:Temperature</measval>
      <plot1name>PID_VaneT_TT20-S</plot1name>
      <plot1ypv>$(pv_group)::PID_VaneT_TT20-S</plot1ypv>
      <plot2name>WtrC-TT-020:Temperature</plot2name>
      <plot2ypv>$(pv_group):WtrC-TT-020:Temperature</plot2ypv>
      <plot3name>WtrC-PCV-020:Opening</plot3name>
      <plot3ypv>$(pv_group):WtrC-PCV-020:Opening</plot3ypv>
      <propor>$(pv_group)::PID_VaneT_Kp</propor>
      <setpoint>$(pv_group)::PID_VaneT_TT20-S</setpoint>
      <title>Vane regulation (°C)</title>
      <valve>$(pv_group):WtrC-PCV-020:Opening</valve>
      <yaxis1>°C</yaxis1>
      <yaxis1_max>35</yaxis1_max>
      <yaxis1_min>25</yaxis1_min>
    </macros>
    <file>EPICS-RegField.bob</file>
    <y>360</y>
    <width>820</width>
    <height>350</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_5</name>
    <macros>
      <deriv>$(pv_group)::PID_VaneDP_Td</deriv>
      <integ>$(pv_group)::PID_VaneDP_Ti</integ>
      <measval>$(pv_group)::FreqDetuned</measval>
      <plot1name>PID_VaneDP_DP-S</plot1name>
      <plot1ypv>$(pv_group)::PID_VaneDP_DP-S</plot1ypv>
      <plot2name>DPhase</plot2name>
      <plot2ypv>$(pv_group)::DPhase</plot2ypv>
      <plot3name>WtrC-PCV-020:Opening</plot3name>
      <plot3ypv>$(pv_group):WtrC-PCV-020:Opening</plot3ypv>
      <propor>$(pv_group)::PID_VaneDP_Kp</propor>
      <setpoint>$(pv_group)::PID_VaneDP_DP-S</setpoint>
      <title>Vane regulation (Detuning)</title>
      <valve>$(pv_group):WtrC-PCV-020:Opening</valve>
      <yaxis1>Hz</yaxis1>
      <yaxis1_max>25000</yaxis1_max>
      <yaxis1_min>-25000</yaxis1_min>
    </macros>
    <file>EPICS-RegField.bob</file>
    <y>720</y>
    <width>820</width>
    <height>350</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_2</name>
    <macros>
      <deriv>$(pv_group)::PID_BodyS1S2_Td</deriv>
      <integ>$(pv_group)::PID_BodyS1S2_Ti</integ>
      <measval>$(pv_group):WtrC-TT-030:Temperature</measval>
      <plot1name>PID_BodyS1S2_TT30-S</plot1name>
      <plot1ypv>$(pv_group)::PID_BodyS1S2_TT30-S</plot1ypv>
      <plot2name>WtrC-TT-030:Temperature</plot2name>
      <plot2ypv>$(pv_group):WtrC-TT-030:Temperature</plot2ypv>
      <plot3name>WtrC-PCV-030:Opening</plot3name>
      <plot3ypv>$(pv_group):WtrC-PCV-030:Opening</plot3ypv>
      <propor>$(pv_group)::PID_BodyS1S2_Kp</propor>
      <setpoint>$(pv_group)::PID_BodyS1S2_TT30-S</setpoint>
      <title>Body S1-S2 regulation loop</title>
      <valve>$(pv_group):WtrC-PCV-030:Opening</valve>
      <yaxis1>°C</yaxis1>
      <yaxis1_max>35</yaxis1_max>
      <yaxis1_min>25</yaxis1_min>
    </macros>
    <file>EPICS-RegField.bob</file>
    <x>840</x>
    <width>820</width>
    <height>350</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_3</name>
    <macros>
      <deriv>$(pv_group)::PID_BodyS3_Td</deriv>
      <integ>$(pv_group)::PID_BodyS3_Ti</integ>
      <measval>$(pv_group):WtrC-TT-040:Temperature</measval>
      <plot1name>PID_BodyS3_TT40-S</plot1name>
      <plot1ypv>$(pv_group)::PID_BodyS3_TT40-S</plot1ypv>
      <plot2name>WtrC-TT-040:Temperature</plot2name>
      <plot2ypv>$(pv_group):WtrC-TT-040:Temperature</plot2ypv>
      <plot3name>WtrC-PCV-040:Opening</plot3name>
      <plot3ypv>$(pv_group):WtrC-PCV-040:Opening</plot3ypv>
      <propor>$(pv_group)::PID_BodyS3_Kp</propor>
      <setpoint>$(pv_group)::PID_BodyS3_TT40-S</setpoint>
      <title>Body S3 regulation loop</title>
      <valve>$(pv_group):WtrC-PCV-040:Opening</valve>
      <yaxis1>°C</yaxis1>
      <yaxis1_max>35</yaxis1_max>
      <yaxis1_min>25</yaxis1_min>
    </macros>
    <file>EPICS-RegField.bob</file>
    <x>840</x>
    <y>360</y>
    <width>820</width>
    <height>350</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_4</name>
    <macros>
      <deriv>$(pv_group)::PID_BodyS4S5_Td</deriv>
      <integ>$(pv_group)::PID_BodyS4S5_Ti</integ>
      <measval>$(pv_group):WtrC-TT-050:Temperature</measval>
      <plot1name>PID_BodyS4S5_TT50-S</plot1name>
      <plot1ypv>$(pv_group)::PID_BodyS4S5_TT50-S</plot1ypv>
      <plot2name>WtrC-TT-050:Temperature</plot2name>
      <plot2ypv>$(pv_group):WtrC-TT-050:Temperature</plot2ypv>
      <plot3name>WtrC-PCV-050:Opening</plot3name>
      <plot3ypv>$(pv_group):WtrC-PCV-050:Opening</plot3ypv>
      <propor>$(pv_group)::PID_BodyS4S5_Kp</propor>
      <setpoint>$(pv_group)::PID_BodyS4S5_TT50-S</setpoint>
      <title>Body S4-S5 regulation loop</title>
      <valve>$(pv_group):WtrC-PCV-050:Opening</valve>
      <yaxis1>°C</yaxis1>
      <yaxis1_max>35</yaxis1_max>
      <yaxis1_min>25</yaxis1_min>
    </macros>
    <file>EPICS-RegField.bob</file>
    <x>840</x>
    <y>720</y>
    <width>820</width>
    <height>350</height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey03-background_14</name>
    <x>20</x>
    <y>1066</y>
    <width>782</width>
    <height>54</height>
    <line_width>0</line_width>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="slide_button" version="2.0.0">
    <name>Slide Button_1</name>
    <pv_name>$(pv_group)::DetuneFallback</pv_name>
    <label>Detune fallback</label>
    <x>370</x>
    <y>1083</y>
    <width>148</width>
    <height>25</height>
    <auto_size>true</auto_size>
  </widget>
  <widget type="combo" version="2.0.0">
    <name>Combo Box_1</name>
    <pv_name>$(pv_group)::DetuneRangeScanTime</pv_name>
    <x>660</x>
    <y>1080</y>
    <width>131</width>
  </widget>
  <widget type="combo" version="2.0.0">
    <name>Combo Box_2</name>
    <pv_name>$(pv_group)::DetuneOption</pv_name>
    <x>160</x>
    <y>1080</y>
    <width>170</width>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>Detuning source:</text>
    <x>30</x>
    <y>1080</y>
    <width>140</width>
    <height>30</height>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_2</name>
    <text>Scan Period</text>
    <x>570</x>
    <y>1083</y>
    <height>30</height>
    <vertical_alignment>1</vertical_alignment>
  </widget>
</display>
